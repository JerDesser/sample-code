package edu.usu.cs.family;

import java.util.LinkedList;
import java.util.List;

public class Person {
	
	String name;
	String middleInitial;
	
	Person parent;
		
	List<Person> childrenList =  new LinkedList<Person>();
	
	public Person(String name, String middleInitial)
	{
		this.name = name;
		this.middleInitial = middleInitial;
	}


	public Person getParent() {
		return parent;
	}

	public void setParent(Person father) {
		this.parent = father;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void addChild(Person child)
	{
		childrenList.add(child);
	}
	
	public Person getOldestAncestor()
	{
		Person temp = this;
		
		while (temp.getParent() != null)
		{
			temp = temp.getParent();
		}
		
		return temp;
	}

}
