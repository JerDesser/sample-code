
public interface Comparer {

	public int compareTo(FootballPlayer fp1, FootballPlayer fp2);
}