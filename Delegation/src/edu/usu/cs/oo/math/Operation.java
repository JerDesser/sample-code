package edu.usu.cs.oo.math;

public enum Operation {

	ADD, 
	SUBTRACT,
	MULTIPLY,
	MOD
}
