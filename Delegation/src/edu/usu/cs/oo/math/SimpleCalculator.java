package edu.usu.cs.oo.math;

public class SimpleCalculator implements Calculator{
	
	public int add(int a, int b)
	{
		int result = a;
		for (int i = 0; i < b; i++)
		{
			result++;
		}

		return result;
	}
	
	public int subtract(int a, int b)
	{
		int result = a - b;
		return result;
	}
	
	public int multiply(int a, int b)
	{
		int result = a * b;
		return result;
	}
	
	public int mod(int a, int b)
	{
		int result = a % b;
		return result;
	}

}
