package edu.usu.cs.oo.log;

public class Logger {
	
	private String name;
	private static Logger instance;
	
	private Logger()
	{
		name = "Name";
	}
	
	public static Logger getInstance(int type)
	{
		if (instance == null)
		{
			instance = new Logger();
		}
		return instance;
	}
	
	public void log(String s) {
		System.out.println(System.currentTimeMillis() + ", " + s);
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Logger other = (Logger) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
